## The Snake Game

The classic snake game I created using:  
- vanilla JavaScript (Object Oriented Programming)  
- Canvas API  
- CSS3 (+BEM)  
- HTML5  

All graphics were created for this project by Rafał Zajączkowski.

You can play the game [here](http://sandramichalska.pl/snake/)

